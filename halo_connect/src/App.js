import React from 'react';
import logo from './logo.svg';
import './App.css';
import Board from './Board'
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      numRows : 6,
      numCols: 7,
      numConnectedToWin: 4
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Connect 4</h1>
          <Board numRows={this.state.numRows} numCols={this.state.numCols} numConnectedToWin={this.state.numConnectedToWin}/>
        </header>
      </div>
    );
  }
}

export default App;

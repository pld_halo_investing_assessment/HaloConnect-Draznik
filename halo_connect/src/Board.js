import React from 'react';


class Board extends React.Component{

    constructor(props) {
        super(props);
        this.numCols = this.props.numCols || 7
        this.numRows = this.props.numRows || 6
        this.numConnectedToWin = this.props.numConnectedToWin || 4
        this.performClickForBoardSpot = this.performClickForBoardSpot.bind(this)
        this.getGameBoardSpotFromMatrixPosition = this.getGameBoardSpotFromMatrixPosition.bind(this)
        this.playerNames = {
            true: this.props.playerOneName | 1,
            false: this.props.playerTwoName | 2
        }
        this.state = {
            currentPlayer:true,
            board: Array.from(Array(this.numCols))
                .map((_, row_i) => {
                    return Array.from(Array(this.numRows))
                    .map((_, col_i) => {
                            return undefined
                        })
                        
                })
        
        }        
      }
    
    getGameBoardSpotFromMatrixPosition(row_i, col_i) {
        /**
         * Map between the board state indicies and the display indices
         */
        return [
            this.numRows - row_i - 1, col_i
        ]
    }

    performClickForBoardSpot(row_i, col_i) {
        /**
         * When a user clicks a spot on the board perform the required actions
         */
        let col_i_placement = col_i
        let row_i_placement = this.getRowToPlacePieceInColumn(col_i_placement)
        this.state.board[col_i_placement][row_i_placement] = this.state.currentPlayer
        if (this.checkForWin(row_i_placement, col_i_placement)){
            alert('Player ' + this.playerNames[this.state.currentPlayer] + ' won!')
        }
        else {
            this.changeCurrentPlayer()
        }
            
    }

    changeCurrentPlayer() {
        /**
         * Toggle the current player
         */
        this.setState(state => ({currentPlayer: !state.currentPlayer}))
        return this.state.currentPlayer
    }

    getRowToPlacePieceInColumn(col) {
        for (var row_i=0; row_i<this.state.board[col].length; row_i++) {    
            if (typeof this.state.board[col][row_i] == 'undefined'){
                return row_i
            }
                
        }
    }
    
    getDirectionPairsToCheck() {
        return [
                [[-1,0], [1, 0]], // NOTE: Left/ Right
                [[0,1], [0,-1]], // NOTE: Up Down
                [[1,1], [-1, -1]], // NOTE: Up Right/ Down Left Diagonal
                [[1,-1], [-1, 1]], // NOTE: Up Left/ Down Right Diagonal
        ]
    }
    
    checkForWin(row_i, col_i){
        /** 
         * Notes:
         *  - Should only check based on the last move played
         *  - Need to Check 4 Directions
        */
       return this.getDirectionPairsToCheck().reduce( (hasWon, directionPair) => {
           return hasWon == true ? hasWon : this.checkForWinInDirectionPair(row_i, col_i, directionPair)
       }, false) 

    }

    addDirectionToPostion(direction, position){
        return [
            direction[0] + position[0],
            direction[1] + position[1]
        ]
        return position.map((positionValue, i) => {
            return positionValue+direction[i]
          })
    }

    isValidAndConnected(row_i, col_i, owner) {
        /**
         * Is in bounds and owned by same person
         */
        try {
            return this.state.board[col_i][row_i] === owner    
        } catch (error) {
            return false
        }
        
    }

    checkForWinInDirectionPair(row_i, col_i, directionPair) {
        /**
         * Notes:
         *  - Need to check in 2 directions
         */
        if( !(Array.isArray(directionPair) && directionPair.length == 2) ) {
            throw "Expected an array of length 2"
        }
        let lastMoveOwner = this.state.board[col_i][row_i]
        
        var numConnected = directionPair.reduce((connected_carry, direction) => {
            var currentPosition = this.addDirectionToPostion(direction, [col_i, row_i]) 
            while (this.isValidAndConnected(currentPosition[1], currentPosition[0], lastMoveOwner) == true) {
                connected_carry += 1
                currentPosition = this.addDirectionToPostion(direction, currentPosition)
            }
            return connected_carry
        }, 1)// NOTE: The initial placement will always count for one
        return numConnected >= this.numConnectedToWin
    }

    displayGameBoard() {
        return (
            <table>
                <tbody>
                {
                    Array.from(Array(this.numRows)).map((_, row_i) => {
                        return (
                            <tr>
                                {
                                    Array.from(Array(this.numCols)).map((_, col_i)=>{
                                        let gameBoardPosition = this.getGameBoardSpotFromMatrixPosition(row_i, col_i)
                                        let gameBoardPositionValue = this.state.board[gameBoardPosition[1]][gameBoardPosition[0]]
                                        let displayValue = typeof gameBoardPositionValue == 'undefined' ? 'O' : (gameBoardPositionValue ? 1: 2) 
                                        return (
                                            <td onClick={_ => { this.performClickForBoardSpot(row_i, col_i) } }>
                                                {displayValue}
                                            </td>
                                        )
                                    })
                                }
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        )
    }

    render() {
       return (
        <div>
            <div>
                Player {this.playerNames[this.state.currentPlayer]}'s turn
            </div>
            <table cellSpacing="0" id="table" border={1}>
            <tbody>{ this.displayGameBoard() }</tbody>
            </table>
        </div>
    )
       }
 }
 export default Board;
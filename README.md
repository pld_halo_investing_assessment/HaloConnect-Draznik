# React JS Connect 4
Please create a single-page web application that allows users to play a simple game of Connect Four

## Getting started
To launch the single-page web application simply run `make start`

## Notes
- Very minimalistic game setup. 
- Testing lacking as I'm not overly familar with reactJS component testing (recent work in python unit testing with pytest)

## Todos
- Allow for dynamic resizing of the board
- Allow for custom player names
- Store history of moves
    - Undo move(s)
- Style board
